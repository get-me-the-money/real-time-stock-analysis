import requests
from pyspark.sql import SparkSession
import pandas as pd
import os
import boto3
spark = SparkSession.builder.getOrCreate()

def get_request(function, symbol, outputsize, datatype, apikey): 
    url = f'https://www.alphavantage.co/query?function={function}&symbol={symbol}&outputsize={outputsize}&datatype={datatype}&apikey={apikey}'
    response = requests.get(url)
    data = response.text
    return data

function = 'TIME_SERIES_DAILY_ADJUSTED'
symbol = 'AMZN'
outputsize = 'full'
datatype = 'csv'    
apikey = 'NNHI6S3QB5GXWLCY'
file_Tem_csv = r'C:\Users\luyan\Stock_Project\File\AMZN_Stock\AMZN_Tem_CSV\AMZN_Tem_Stock.csv'
file_csv = r'C:\Users\luyan\Stock_Project\File\AMZN_Stock\AMZN_CSV\AMZN_Stock.csv'

data = get_request(function, symbol, outputsize, datatype, apikey)

def download_stock(data, file_Tem_csv):
    with open(file_Tem_csv, "w", newline='') as file:
        file.write(data)
    print("Successfully download", file_Tem_csv)

download_stock(data, file_Tem_csv)

def append_new_data(file_Tem_csv, file_csv):
    df_new = pd.read_csv(file_Tem_csv, header=0)
    df_append = df_new.head(1)
    

    if not os.path.isfile(file_csv):
        df_new['timestamp'] = pd.to_datetime(df_new['timestamp'], format="%Y-%m-%d")
        df_new.to_csv(file_csv, index=False)
        print("Created a new CSV file:", file_csv)
    else:
        df_old = pd.read_csv(file_csv, header=0)
    

        first_row_date =df_old['timestamp'].iloc[0]
        append_date = df_append['timestamp'].iloc[0]
        
        if  first_row_date == append_date:
            print("The data didn't change")
        else:    
            df_old['timestamp'] = pd.to_datetime(df_old['timestamp'], format="%Y/%m/%d")
            df_updated = pd.concat([df_old, df_append], ignore_index=True)
            df_updated['timestamp'] = pd.to_datetime(df_updated['timestamp'], format="%Y-%m-%d")
            df_updated = df_updated.sort_values(by='timestamp', key=lambda x: pd.to_datetime(x),ascending=False)
            df_updated.to_csv(file_csv, header=True, index=False)
            print("Appended new data to CSV file:", file_csv)

append_new_data(file_Tem_csv, file_csv)




s3 = boto3.client(
    's3',
    region_name='us-east-2',
    aws_access_key_id='AKIAULDUSLD3UQIIXGOJ',
    aws_secret_access_key='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')
s3_bucket = 'stock214451'
s3_key = 'CSV/AMZN_TIME_SERIES_DAILY_ADJUSTED_CSV/AMZN_Stock.csv'


def delete_objects_in_S3_Parquet_folder(bucket_name, folder_prefix):
    response = s3.list_objects_v2(Bucket=bucket_name, Prefix=folder_prefix)

    if 'Contents' in response:
        objects = response['Contents']
        object_keys = [obj['Key'] for obj in objects if obj['Key'] != folder_prefix]

        for key in object_keys:
            s3.delete_object(Bucket=bucket_name, Key=key)
        
        print(f"Deleted objects in folder: s3://{bucket_name}/{folder_prefix}")
    else:
        print(f"No folder found : s3://{bucket_name}/{folder_prefix}")

delete_objects_in_S3_Parquet_folder('stock214451', 'PARQUET/AMZN_TIME_SERIES_DAILY_ADJUSTED_PARQUET/')




def append_new_data_to_s3_Csv(file_Tem_csv, s3_bucket, s3_key):
    df_new = pd.read_csv(file_Tem_csv, header=0)
    df_append = df_new.head(1)

    try:
        s3.head_object(Bucket=s3_bucket, Key=s3_key)
        file_exists = True
    except:
        file_exists = False

    if not file_exists:
        df_new['timestamp'] = pd.to_datetime(df_new['timestamp'], format="%Y-%m-%d")
        csv_data = df_new.to_csv(index=False).encode()
        s3.put_object(Body=csv_data, Bucket=s3_bucket, Key=s3_key)
        print("Created a new S3 file:", s3_key)
    else:
        
        obj = s3.get_object(Bucket=s3_bucket, Key=s3_key)
        df_old = pd.read_csv(obj['Body'])
        first_row_date =df_old['timestamp'].iloc[0]
        append_date = df_append['timestamp'].iloc[0]

        if  first_row_date == append_date:
            print("The S3 File data didn't change")
        else:
            df_old['timestamp'] = pd.to_datetime(df_old['timestamp'], format="%Y/%m/%d")
            df_updated = pd.concat([df_old, df_append], ignore_index=True)
            df_updated['timestamp'] = pd.to_datetime(df_updated['timestamp'], format="%Y-%m-%d")
            df_updated = df_updated.sort_values(by='timestamp', key=lambda x: pd.to_datetime(x),ascending=False)
            csv_data = df_updated.to_csv(index=False).encode()
            s3.put_object(Body=csv_data, Bucket=s3_bucket, Key=s3_key)
            print("Appended new data to S3 file:", s3_key)
            

append_new_data_to_s3_Csv(file_Tem_csv, s3_bucket, s3_key)




glue_client = boto3.client('glue', region_name='us-east-2')

def start_glue_job(job_name):
      
    try:
        response = glue_client.start_job_run(JobName=job_name)
        job_run_id = response['JobRunId']
        print(f"Glue Job '{job_name}' started successfully. JobRunId: {job_run_id}")
        return job_run_id
    except Exception as e:
        print(f"Failed to start Glue Job '{job_name}': {str(e)}")
    

start_glue_job('AMZN_TIME_SERIES_DAILY_ADJUSTED_CSV_To_PARQUET')

